package counter

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)
type HandlerService interface{
	IncreaseNumber(i int64) *int64
	DecreaseNumber(i int64) *int64
}

type Handler struct{
	Service HandlerService
}

func NewHandler(s HandlerService) *Handler{
	return &Handler{
		Service: s,
	}
}

func (h *Handler) SetupRoutes(app *fiber.App){
	app.Get("/counter/increment/:number",h.IncreaseNumber)
	app.Get("/counter/decrement/:number",h.DecreaseNumber)
}

func (h *Handler) IncreaseNumber(c *fiber.Ctx) error {
	number,_ := strconv.ParseInt(c.Params("number"),10,64)

	result := h.Service.IncreaseNumber(number)

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"data": &result ,
	})

}

func (h *Handler) DecreaseNumber(c *fiber.Ctx) error {
	number,_ := strconv.ParseInt(c.Params("number"),10,64)

	result := h.Service.DecreaseNumber(number)

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"data": &result ,
	})
}