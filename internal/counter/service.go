package counter

type Service struct{
	
}

func NewService() *Service{
	return &Service{}
}

func (s *Service) IncreaseNumber(i int64) *int64{
	i++
	return &i
}

func (s *Service) DecreaseNumber(i int64) *int64{
	i--
	return &i
}