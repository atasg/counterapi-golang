package main

import (
	"counter/internal/counter"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func CreateApp() *fiber.App{
	service := counter.NewService()
	handler := counter.NewHandler(service)
	app:= fiber.New()
	handler.SetupRoutes(app)
	return app
}

func main() {
	app := CreateApp()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "*",
		AllowCredentials: true,
	}))

	app.Listen(":8080")
}
